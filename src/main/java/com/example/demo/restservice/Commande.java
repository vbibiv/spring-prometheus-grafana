package com.example.demo.restservice;

import lombok.Data;

@Data
public class Commande {
    private String id;
    private String product;
    private String customerLastname;
    private String customerName;
    private String customerStreet;
    private String customerCity;
    private String customerPostCode;
    private String customerCountry;
}
