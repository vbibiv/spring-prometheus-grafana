package com.example.demo.restservice;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class CommandeController {

    public final List<Commande> cmdArray = new ArrayList<Commande>();

    @Autowired
    private MeterRegistry meterRegistry;


    @PostMapping("/commande")
    public Commande createCommande(@RequestBody Commande commande) {
        meterRegistry.counter("order_counter").increment();
        commande.setId(UUID.randomUUID().toString());
        cmdArray.add(commande);
        return commande;
    }
}
